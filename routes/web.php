<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/register', 'AuthController@register')->name('register');
Route::post('/register', 'AuthController@register');

Route::get('/forgot-password', 'AuthController@forgotPassword')->name('forgot-password');
Route::post('/forgot-password', 'AuthController@forgotPassword');

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('dashboard.layouts.main');
    })->name('Overview');
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@index')->name('Administrator - Users');
        Route::get('/create', 'UserController@create')->name('users Create New');
        Route::post('/', 'UserController@store')->name('users.store');
        Route::get('/{id}', 'UserController@show')->name('users.show');
        Route::get('/{id}/edit', 'UserController@edit')->name('users Update');
        Route::patch('/{id}', 'UserController@update')->name('users.update');
        Route::delete('/{id}', 'UserController@destroy')->name('users.destroy');
    });
    Route::group(['prefix' => 'blogs'], function () {
        Route::get('/', 'BlogController@index')->name('Blogs');
        Route::get('/create', 'BlogController@create')->name('blogs Create New');
        Route::post('/', 'BlogController@store')->name('blogs.store');
        Route::get('/{slug}', 'BlogController@show')->name('blogs.show');
        Route::get('/{slug}/edit', 'BlogController@edit')->name('blogs Update');
        Route::put('/{slug}', 'BlogController@update')->name('blogs.update');
        Route::delete('/{slug}', 'BlogController@destroy')->name('blogs.destroy');
    });

    Route::group(['prefix' => 'kategoris'], function () {
        Route::get('/', 'KategoriController@index')->name('kategoris.index');
        Route::get('/create', 'KategoriController@create')->name('kategoris Create New');
        Route::post('/', 'KategoriController@store')->name('kategoris.store');
        Route::get('/{id}', 'KategoriController@show')->name('kategoris.show');
        Route::get('/{id}/edit', 'KategoriController@edit')->name('kategoris.edit');
        Route::put('/{id}', 'KategoriController@update')->name('kategoris.update');
        Route::delete('/{id}', 'KategoriController@destroy')->name('kategoris.destroy');
    });

    Route::group(['prefix' => 'jobvacancies'], function () {
        Route::get('/', 'JobVacancyController@index')->name('Job Vacancy');
        Route::get('/create', 'JobVacancyController@create')->name('jobvacancies Create New');
        Route::post('/', 'JobVacancyController@store')->name('jobvacancies.store');
        Route::get('/{slug}', 'JobVacancyController@show')->name('jobvacancies.show');
        Route::get('/{slug}/edit', 'JobVacancyController@edit')->name('jobvacancies Update');
        Route::put('/{slug}', 'JobVacancyController@update')->name('jobvacancies.update');
        Route::delete('/{slug}', 'JobVacancyController@destroy')->name('jobvacancies.destroy');
    });

    Route::group(['prefix' => 'candidates'], function () {
        Route::get('/', 'CandidateController@index')->name('Candidates');
        Route::get('/create', 'CandidateController@create')->name('candidates Create New');
        Route::post('/', 'CandidateController@store')->name('candidates.store');
        Route::get('/{id}', 'CandidateController@show')->name('candidates.show');
        Route::get('/{id}/edit', 'CandidateController@edit')->name('candidates Update');
        Route::put('/{id}', 'CandidateController@update')->name('candidates.update');
        Route::delete('/{id}', 'CandidateController@destroy')->name('candidates.destroy');
    });

    // Route untuk eventCategory
    Route::group(['prefix' => 'event-categories'], function () {
        Route::get('/', 'EventCategoryController@index')->name('event_categories.index');
        Route::get('/create', 'EventCategoryController@create')->name('event_categories Create New');
        Route::post('/', 'EventCategoryController@store')->name('event_categories.store');
        Route::get('/{slug}', 'EventCategoryController@show')->name('event_categories.show');
        Route::get('/{slug}/edit', 'EventCategoryController@edit')->name('event_categories Update');
        Route::put('/{slug}', 'EventCategoryController@update')->name('event_categories.update');
        Route::delete('/{slug}', 'EventCategoryController@destroy')->name('event_categories.destroy');
    });

    Route::group(['prefix' => 'events'], function () {
        // Route untuk event
        Route::get('/', 'EventController@index')->name('Events');
        Route::get('/create', 'EventController@create')->name('events Create New');
        Route::post('/', 'EventController@store')->name('events.store');
        Route::get('/{slug}', 'EventController@show')->name('events.show');
        Route::get('/{slug}/edit', 'EventController@edit')->name('events Update');
        Route::put('/{slug}', 'EventController@update')->name('events.update');
        Route::delete('/{slug}', 'EventController@destroy')->name('events.destroy');
    });
    Route::group(['prefix' => 'companies'], function () {
        Route::get('/', 'CompanyController@index')->name('Companies');
        Route::get('/create', 'CompanyController@create')->name('companies Create New');
        Route::post('/', 'CompanyController@store')->name('companies.store');
        Route::get('/{id}', 'CompanyController@show')->name('companies.show');
        Route::get('/{id}/edit', 'CompanyController@edit')->name('companies Update');
        Route::put('/{id}', 'CompanyController@update')->name('companies.update');
        Route::delete('/{id}', 'CompanyController@destroy')->name('companies.destroy');
    });
    Route::group(['prefix' => 'employees_categories'], function () {
        Route::get('/', 'EmployeesCategoryController@index')->name('employees_categories.index');
        Route::get('/create', 'EmployeesCategoryController@create')->name('employees_categories Create New');
        Route::post('/', 'EmployeesCategoryController@store')->name('employees_categories.store');
        Route::get('/{employeesCategory}', 'EmployeesCategoryController@show')->name('employees_categories.show');
        Route::get('/{employeesCategory}/edit', 'EmployeesCategoryController@edit')->name('employees_categories Update');
        Route::put('/{employeesCategory}', 'EmployeesCategoryController@update')->name('employees_categories.update');
        Route::delete('/{employeesCategory}', 'EmployeesCategoryController@destroy')->name('employees_categories.destroy');
    });
    Route::group(['prefix' => 'employees'], function () {
        Route::get('/', 'EmployeeController@index')->name('Employees');
        Route::get('/create', 'EmployeeController@create')->name('employees Create New');
        Route::post('/', 'EmployeeController@store')->name('employees.store');
        Route::get('/{employee}', 'EmployeeController@show')->name('employees.show');
        Route::get('/{employee}/edit', 'EmployeeController@edit')->name('employees Update');
        Route::put('/{employee}', 'EmployeeController@update')->name('employees.update');
        Route::delete('/{employee}', 'EmployeeController@destroy')->name('employees.destroy');
    });
    Route::group(['prefix' => 'sales'], function () {
        Route::get('/', 'SaleController@index')->name('Sales Report');
        Route::get('/create', 'SaleController@create')->name('sales Create New');
        Route::post('/', 'SaleController@store')->name('sales.store');
        Route::get('/{id}', 'SaleController@show')->name('sales.show');
        Route::get('/{id}/edit', 'SaleController@edit')->name('sales Update');
        Route::put('/{id}', 'SaleController@update')->name('sales.update');
        Route::delete('/{id}', 'SaleController@destroy')->name('sales.destroy');
    });
});
Route::middleware(['addTokenToSalesRoute'])->group(function () {
    Route::group(['prefix' => 'v'], function () {
        Route::group(['prefix' => 'sales'], function () {
            Route::get('/', 'SaleController@globalIndex')->name('Sales Visiting');
            Route::post('/', 'SaleController@storeGlobalIndex')->name('salesGlobal.store');
        });
    });
});
