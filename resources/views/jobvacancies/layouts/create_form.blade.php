<section class="bg-gray-50 dark:bg-gray-900">
    <div class="mx-auto">
        <div class="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
            <h2 class="text-2xl dark:text-white mt-3 mx-4 font-medium">Add a new job vacancy</h2>
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                <div class="w-full">
                    <form action="{{ route('jobvacancies.store') }}" method="POST">
                        @csrf
                        <div class="grid gap-4 sm:grid-cols-2 sm:gap-6">
                            <div class="sm:col-span-2">
                                <label for="title"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Nama
                                    Pekerjaan</label>
                                <input type="text" tabindex="4" name="title" id="title"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                            <div class="w-full">
                                <label for="Deskripsi Pekerjaan"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Deskripsi
                                    Pekerjaan</label>
                                <x-head.tinymce-config />
                                <textarea id="myeditorinstance" tabindex="0" rows="10"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Write your thoughts here..." name="description" required></textarea>
                            </div>
                            <div class="w-full">
                                <label for="requirement"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Requirements</label>
                                <textarea id="myeditorinstance" tabindex="3" rows="10"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Write your thoughts here..." name="requirement" required></textarea>
                            </div>
                            <div class="w-full">
                                <label for="expected_from_candidate"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Expected
                                    from candidates</label>
                                <textarea id="myeditorinstance" tabindex="2" rows="10"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Write your thoughts here..." name="expected_from_candidate" required></textarea>
                            </div>
                            <div class="w-full">
                                <label for="what_candidate_got"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">What
                                    candidate got</label>
                                <textarea id="myeditorinstance" tabindex="3" rows="10"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Write your thoughts here..." name="what_candidate_got" required></textarea>
                            </div>
                            <div class="w-full">
                                <label for="location"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Job
                                    Location</label>
                                <select id="location" name="location" tabindex="5"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    required>
                                    <option selected>Pilih lokasi</option>
                                    <option value="Jakarta, DKI Jakarta, Indonesia">Wilayah Pusat</option>
                                    <option value="Medan, Sumatera Utara, Indonesia">Wilayah Medan</option>
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="salary"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Salary
                                    Offer</label>
                                <input type="number" name="salary" id="salary" tabindex="6" step="0.01"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                            <div class="sm:col-span-2">
                                <label for="salary"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Deadline
                                    date</label>
                                <input type="date" name="deadline" id="deadline" tabindex="7"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                        </div>
                        <button type="submit"
                            class="mt-5 flex items-center justify-center text-white bg-[#f95b12] hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus:ring-primary-800">
                            <svg class="h-3.5 w-3.5 text-gray-800 dark:text-white mr-2" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 19">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M15 15h.01M4 12H2a1 1 0 0 0-1 1v4a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1h-3M9.5 1v10.93m4-3.93-4 4-4-4" />
                            </svg>
                            Save
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
