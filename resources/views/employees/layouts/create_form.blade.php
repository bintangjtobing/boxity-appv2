<section class="bg-gray-50 dark:bg-gray-900">
    <div class="mx-auto">
        <div class="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
            <h2 class="text-2xl dark:text-white mt-3 mx-4 font-medium">Add a new employee</h2>
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                <div class="w-full">
                    <form action="{{ route('employees.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="grid gap-4 sm:grid-cols-2 sm:gap-6">
                            <div class="sm:col-span-2">
                                <label for="name"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Nama
                                    Lengkap Karyawan</label>
                                <input type="text" name="name" id="name"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="email"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Email
                                </label>
                                <input type="email" name="email" id="email"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="phone_number"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">No.
                                    Telepon</label>
                                <input type="phone" name="phone_number" id="phone_number"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="company_id"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Company</label>
                                <select id="company_id" name="company_id"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih perusahaan</option>
                                    @foreach ($companies as $comp)
                                        <option value="{{ $comp->id }}">{{ $comp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="job_title_category_id"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kategori
                                    Karyawan</label>
                                <select id="job_title_category_id" name="job_title_category_id"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih kategori karyawan</option>
                                    @foreach ($jobCategories as $kategori)
                                        <option value="{{ $kategori->id }}">{{ $kategori->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="job_title"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Jenis
                                    Karyawan</label>
                                <select id="job_title" name="job_title"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih jenis karyawan</option>
                                    <option value="Internship">Internship</option>
                                    <option value="Full-time work">Full-time work</option>
                                    <option value="Part-time work">Part-time work</option>
                                    <option value="Freelance">Freelance</option>
                                    <option value="Volunteer">Volunteer</option>
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="employment_status"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Status
                                    Karyawan</label>
                                <select id="employment_status" name="employment_status"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih status karyawan</option>
                                    <option value="Permanent">Permanent</option>
                                    <option value="Contract Based">Contract Based</option>
                                    <option value="Probation">Probation</option>
                                    <option value="Temporary">Temporary</option>
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="hire_date"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Hire
                                    Date</label>
                                <input type="date" name="hire_date" id="hire_date"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="termination_date"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Termination
                                    Date</label>
                                <input type="date" name="termination_date" id="termination_date"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="">
                            </div>
                            <div class="w-full">
                                <label for="address"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Full
                                    Address</label>
                                <input type="text" name="address" id="address"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                            <div class="w-full">
                                <label for="city"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">City</label>
                                <input type="text" list="cities" id="cityInput" name="city"
                                    placeholder="Pilih atau ketik untuk mencari kota..."
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <datalist id="cities" name="cities">
                                    <option value="" selected>Pilih kota</option>
                                    @foreach ($kota as $item)
                                        <option value="{{ $item['asciiname'] }}">{{ $item['asciiname'] }}</option>
                                    @endforeach
                                </datalist>
                            </div>

                            <div class="w-full">
                                <label for="province"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Provinsi</label>
                                <select id="province" name="province"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected disabled hidden>Pilih Provinsi</option>
                                    @foreach ($provinsi as $item)
                                        <option value="{{ $item['name'] }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="postal_code"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Postal
                                    Code</label>
                                <input type="text" name="postal_code" id="postal_code"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="country"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Negara</label>
                                <input type="text" name="country" id="country"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="Indonesia" required="">
                            </div>
                            <hr class="sm:col-span-2">
                            <h2 class="text-2xl dark:text-white mt-3 mx-4 font-medium">Daftar Kontak Darurat</h2>
                            <div class="sm:col-span-2">
                                <label for="emergency_contact_name"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama Kontak
                                    Darurat</label>
                                <input type="text" name="emergency_contact_name" id="emergency_contact_name"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                            <div class="sm:col-span-2">
                                <label for="emergency_contact_phone_number"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nomor Kontak
                                    Darurat</label>
                                <input type="number" name="emergency_contact_phone_number"
                                    id="emergency_contact_phone_number"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required>
                            </div>
                        </div>
                        <button type="submit"
                            class="mt-5 flex items-center justify-center text-white bg-[#f95b12] hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus-ring-primary-800">
                            <svg class="h-3.5 w-3.5 text-gray-800 dark:text-white mr-2" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 19">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M15 15h.01M4 12H2a1 1 0 0 0-1 1v4a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1h-3M9.5 1v10.93m4-3.93-4 4-4-4" />
                            </svg>
                            Save
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // Script untuk pencarian kota
    const cityInput = document.getElementById('cityInput');
    const citiesDatalist = document.getElementById('cities');

    cityInput.addEventListener('input', function() {
        const inputText = this.value.toLowerCase();
        const cityOptions = document.querySelectorAll('#cities option');
        cityOptions.forEach(option => {
            const optionText = option.value.toLowerCase();
            if (optionText.startsWith(inputText)) {
                option.hidden = false;
            } else {
                option.hidden = true;
            }
        });
    });

    citiesDatalist.addEventListener('change', function() {
        cityInput.value = this.value;
    });
</script>
