@extends('auth.layouts.main')

@section('container')
    <section class="bg-gray-50 dark:bg-gray-900">
        <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto h-screen md:h-screen lg:py-0">
            <a href="#" class="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                <img class="h-16 mr-2"
                    src="https://res.cloudinary.com/boxity-id/image/upload/v1703264204/resources_new_boxity/znhxlwmqju2txa09bsmm.png"
                    alt="Boxity Int Logo Official">
            </a>
            <div
                class="w-full rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                    <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                        Masuk Ke Akun Anda
                    </h1>
                    @if (session('alert-danger'))
                        <div class="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-900 dark:text-red-400"
                            role="alert">
                            <span class="font-medium"><b>Unsuccessfull</b></span><br>Email atau password salah / User belum
                            diotorisasi. <br> Silahkan
                            coba lagi.
                        </div>
                    @endif
                    <form class="space-y-4 md:space-y-6" method="POST" action="/login">
                        @csrf
                        <div>
                            <label for="email"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Email</label>
                            <input type="email" name="email" id="email"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                placeholder="company@company.com" required="">
                        </div>
                        <div>
                            <label for="password"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Kata
                                Sandi</label>
                            <input type="password" name="password" id="password" placeholder="••••••••"
                                class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-[#9345a3] focus:border-[#9345a3] block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-[#9345a3] dark:focus:border-[#9345a3]"
                                required="">
                        </div>
                        <button type="submit"
                            class="bg-[#f95b12] w-full text-white focus:ring-4 focus:outline-none focus:ring-[#d24f98] font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-[#d24f98]">Masuk</button>
                        <div class="flex justify-between">
                            <p class="text-xs lg:text-sm font-light text-gray-500 dark:text-gray-400">
                                Belum Punya Akun? <a class="text-primary-color" href="/register"
                                    class="font-medium hover:underline"><abbr title="Register here">Daftar Disini</abbr></a>
                            </p>
                            <a href="/forgot-password"
                                class="text-xs lg:text-sm font-light text-gray-500 dark:text-gray-400"><abbr
                                    title="Forgot Password">Lupa
                                    Password</abbr></a>
                        </div>
                    </form>
                </div>
            </div>
            <p class="text-sm text-gray-700 my-5">© Copyright <?php $Y = date('Y');
            echo $Y; ?> <abbr title="BoxityID">Boxity Central
                    Indonesia, PT</abbr></p>
        </div>
    </section>
@endsection
