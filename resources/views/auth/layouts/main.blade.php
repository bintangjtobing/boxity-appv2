<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.css"  rel="stylesheet" />
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="icon" href="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png" type="image/png">
    <title>Boxity Auth - Login</title>
</head>
<body>
    <div style="min-height: 100vh" class="antialiased bg-gray-50 dark:bg-gray-900">
        <div>
            @yield('container')
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.js"></script>   
</body>
</html>