@extends('v.index')
@section('container')
    <section class="bg-gray-50 dark:bg-gray-900">
        <div class="py-8 px-4 mx-auto max-w-2xl lg:py-16">
            <a href="/v/sales" class="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                <img class="h-16 mr-2"
                    src="https://res.cloudinary.com/boxity-id/image/upload/v1703264204/resources_new_boxity/znhxlwmqju2txa09bsmm.png"
                    alt="Boxity Int Logo Official">
            </a>
            @if (session('success'))
                <div id="alert-sucess"
                    class="flex items-center p-4 mb-4 text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400"
                    role="alert">
                    <svg class="flex-shrink-0 w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="currentColor" viewBox="0 0 20 20">
                        <path
                            d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z" />
                    </svg>
                    <span class="sr-only">Info</span>
                    <div class="ms-3 text-sm font-medium">
                        {{ session('success') }}
                    </div>
                    <button type="button"
                        class="ms-auto -mx-1.5 -my-1.5 bg-green-50 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex items-center justify-center h-8 w-8 dark:bg-gray-800 dark:text-green-400 dark:hover:bg-gray-700"
                        data-dismiss-target="#alert-sucess" aria-label="Close">
                        <span class="sr-only">Close</span>
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                        </svg>
                    </button>
                </div>
            @elseif (session('error'))
                <div id="alert-danger"
                    class="flex items-center p-4 mb-4 text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
                    role="alert">
                    <svg class="flex-shrink-0 w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg"
                        fill="currentColor" viewBox="0 0 20 20">
                        <path
                            d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z" />
                    </svg>
                    <span class="sr-only">Info</span>
                    <div class="ms-3 text-sm font-medium">
                        {{ session('error') }}
                    </div>
                    <button type="button"
                        class="ms-auto -mx-1.5 -my-1.5 bg-red-50 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex items-center justify-center h-8 w-8 dark:bg-gray-800 dark:text-red-400 dark:hover:bg-gray-700"
                        data-dismiss-target="#alert-sucess" aria-label="Close">
                        <span class="sr-only">Close</span>
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                        </svg>
                    </button>
                </div>
            @endif
            <div id="accordion-collapse" data-accordion="collapse" class="my-5">
                <h2 id="accordion-collapse-heading-1">
                    <button type="button"
                        class="flex items-center justify-between w-full p-5 font-medium rtl:text-right text-gray-500 border border-b-0 border-gray-200 rounded-t-xl focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800 gap-3"
                        data-accordion-target="#accordion-collapse-body-1" aria-expanded="true"
                        aria-controls="accordion-collapse-body-1">
                        <span>Tujuan</span>
                        <svg data-accordion-icon class="w-3 h-3 rotate-180 shrink-0" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M9 5 5 1 1 5" />
                        </svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-1" class="hidden" aria-labelledby="accordion-collapse-heading-1">
                    <div class="p-5 border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                        <ul class="max-w-lg space-y-1 text-gray-500 list-disc list-inside dark:text-gray-400">
                            <li>Mengumpulkan informasi tentang calon klien, termasuk kebutuhan, tantangan, dan tujuan
                                mereka.</li>
                            <li>Membangun hubungan dengan calon klien dan menciptakan kepercayaan.</li>
                            <li>Mempresentasikan produk atau layanan perusahaan dan menjelaskan bagaimana produk atau
                                layanan
                                tersebut
                                dapat memenuhi kebutuhan calon klien.</li>
                            <li>Menilai kelayakan calon klien dan menentukan apakah mereka merupakan prospek yang baik.</li>
                        </ul>
                    </div>
                </div>
                <h2 id="accordion-collapse-heading-2">
                    <button type="button"
                        class="flex items-center justify-between w-full p-5 font-medium rtl:text-right text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800 gap-3"
                        data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                        aria-controls="accordion-collapse-body-2">
                        <span>Tugas</span>
                        <svg data-accordion-icon class="w-3 h-3 rotate-180 shrink-0" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M9 5 5 1 1 5" />
                        </svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                    <div class="p-5 border border-b-0 border-gray-200 dark:border-gray-700">
                        <ul class="max-w-lg space-y-1 text-gray-500 list-disc list-inside dark:text-gray-400">
                            <li>Lakukan riset tentang calon klien sebelum kunjungan. Ini akan membantu Anda untuk memahami
                                kebutuhan
                                dan
                                tantangan mereka.</li>
                            <li>Persiapkan presentasi yang menarik dan informatif yang menjelaskan bagaimana produk atau
                                layanan
                            </li>
                            <li>Anda dapat
                                memenuhi kebutuhan calon klien.</li>
                            <li>Datang tepat waktu dan berpakaian profesional.</li>
                            <li>Bicaralah dengan percaya diri dan antusias.</li>
                            <li>Dengarkan dengan cermat apa yang dikatakan calon klien.</li>
                            <li>Tanyakan pertanyaan yang tepat untuk memahami kebutuhan mereka dengan lebih baik.</li>
                            <li>Jawab pertanyaan calon klien dengan jelas dan ringkas.</li>
                            <li>Berikan tindak lanjut yang jelas kepada calon klien.</li>
                        </ul>
                    </div>
                </div>
                <h2 id="accordion-collapse-heading-3">
                    <button type="button"
                        class="flex items-center justify-between w-full p-5 font-medium rtl:text-right text-gray-500 border border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800 gap-3"
                        data-accordion-target="#accordion-collapse-body-3" aria-expanded="false"
                        aria-controls="accordion-collapse-body-3">
                        <span>Semangat!!</span>
                        <svg data-accordion-icon class="w-3 h-3 rotate-180 shrink-0" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M9 5 5 1 1 5" />
                        </svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-3" class="hidden" aria-labelledby="accordion-collapse-heading-3">
                    <div class="p-5 border border-t-0 border-gray-200 dark:border-gray-700">
                        <p class="mb-2 text-gray-500 dark:text-gray-400">The only limit to our realization of tomorrow will
                            be our doubts of today.
                            Satu-satunya batasan untuk realisasi kita di hari esok adalah keraguan kita hari ini.
                            Franklin D. Roosevelt, 1933, First Inaugural Address
                            <a href="https://instagram.com/boxityid"
                                class="font-semibold text-gray-900 underline dark:text-white decoration-green-500">Semangat
                                ya
                                #temanBoxity</a>
                    </div>
                </div>
            </div>

            <h1 class="mb-4 text-xl font-bold text-gray-900 dark:text-white">Add a new prospect sales</h1>
            <div class="w-full">
                <form action="{{ route('salesGlobal.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="grid gap-4 sm:grid-cols-2 sm:gap-6">
                        <div class="sm:col-span-2">
                            <label for="kode_sales"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">No.
                                Anggota Sales Boxity</label>
                            <input type="text" list="user_sales" id="salesInput" name="kode_sales"
                                placeholder="Pilih atau ketik untuk mencari kode/nama anggota..."
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <datalist id="user_sales" name="user_sales">
                                <option value="" selected>Pilih user</option>
                                @foreach ($users as $item)
                                    <option value="{{ $item['name'] }}" data-kode="{{ $item['kode_anggota'] }}">
                                        {{ $item['name'] }}</option>
                                @endforeach
                            </datalist>
                        </div>
                        <div class="sm:col-span-2">
                            <label for="name"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Nama
                                Usaha Bisnis / Perusahaan</label>
                            <input type="text" name="name" id="name"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="" required="">
                        </div>
                        <div class="w-full">
                            <label for="email"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Email
                                Bisnis</label>
                            <input type="email" name="email" id="email"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="" required="">
                        </div>
                        <div class="w-full">
                            <label for="phone_number"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">No.
                                Telepon</label>
                            <input type="phone" name="phone_number" id="phone_number"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="" required="">
                        </div>
                        <div class="sm:col-span-2">
                            <label for="territory"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Alamat
                                Lengkap</label>
                            <input type="text" name="territory" id="territory"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="Alamat lengkap ex: Jalan, Kecamatan, Kelurahan, Kode Pos" required="">
                        </div>
                        <div class="w-full">
                            <label for="date"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Tanggal
                                Kunjungan</label>
                            <input type="date" name="date" id="date"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="" required="">
                        </div>
                        <div class="w-full">
                            <label for="time"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Waktu
                                Kunjungan</label>
                            <input type="time" name="time" id="time"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                placeholder="" required="">
                        </div>
                        <div class="sm:col-span-2">
                            <label for="description"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Kesimpulan</label>
                            <x-head.tinymce-config />
                            <textarea id="myeditorinstance" rows="10" tabindex="1"
                                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Simpulkan kebutuhan, masalah, dan solusi calon customer/client" name="notes"></textarea>
                        </div>
                        <div class="sm:col-span-2">
                            <label for="gambar-cover"
                                class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Bukti
                                Foto Kunjungan</label>
                            <label for="bukti_foto_kunjungan"
                                class="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                                <div class="flex flex-col items-center justify-center pt-5 pb-6">
                                    <img id="previewImage" class="w-32 h-32 mb-4 rounded-lg object-cover"
                                        src="https://res.cloudinary.com/boxity-id/image/upload/e_colorize:100/v1704973935/camera-icon-21_ylr9le.png">
                                    <p class="mb-2 text-sm text-gray-500 dark:text-gray-400"><span
                                            class="font-semibold">Click to upload</span> or drag and drop</p>
                                    <p class="text-xs text-gray-500 dark:text-gray-400">SVG, PNG, JPG, or GIF (MAX. 10MB)
                                    </p>
                                </div>
                                <input id="bukti_foto_kunjungan" accept="image/*" type="file" class="hidden"
                                    name="bukti_foto_kunjungan" />
                            </label>
                        </div>
                    </div>
                    <button type="submit"
                        class="mt-5 flex items-center justify-center text-white bg-[#f95b12] hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus-ring-primary-800">
                        <svg class="h-3.5 w-3.5 text-gray-800 dark:text-white mr-2" aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 19">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M15 15h.01M4 12H2a1 1 0 0 0-1 1v4a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1h-3M9.5 1v10.93m4-3.93-4 4-4-4" />
                        </svg>
                        Save
                    </button>
                </form>
            </div>
            <p class="text-sm text-gray-700 my-5">© Copyright <?php $Y = date('Y');
            echo $Y; ?> <abbr title="BoxityID">Boxity Central
                    Indonesia, PT</abbr></p>
        </div>
    </section>
    <!-- ... your HTML content ... -->

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const salesInput = document.getElementById('salesInput');
            const salesDataList = document.getElementById('user_sales');

            salesInput.addEventListener('input', function() {
                const inputText = this.value.toLowerCase();
                const salesOptions = document.querySelectorAll('#user_sales option');

                salesOptions.forEach(option => {
                    const optionText = option.value.toLowerCase();
                    if (optionText.startsWith(inputText)) {
                        option.hidden = false;
                    } else {
                        option.hidden = true;
                    }
                });
            });

            salesInput.addEventListener('change', function() {
                const selectedOption = salesDataList.querySelector(`option[value="${salesInput.value}"]`);

                if (selectedOption) {
                    const kodeAnggota = selectedOption.getAttribute('data-kode');
                    salesInput.value =
                        kodeAnggota; // Set the value of kode_sales to the selected kode_anggota
                    salesInput.setAttribute('readonly', 'readonly'); // Make the input read-only
                }
            });

            // Additional code to handle touch events for mobile browsers
            salesInput.addEventListener('touchstart', function() {
                // Remove the 'readonly' attribute on touch to allow keyboard input
                salesInput.removeAttribute('readonly');
            });

            const buktiFotoInput = document.getElementById('bukti_foto_kunjungan');
            buktiFotoInput.addEventListener('change', function(event) {
                var input = event.target;
                var reader = new FileReader();

                reader.onload = function() {
                    var preview = document.getElementById('previewImage');
                    preview.src = reader.result;
                };

                reader.readAsDataURL(input.files[0]);
            });

            // Get the current URL
            var currentUrl = window.location.href;

            // Check if the URL already contains a token
            if (!currentUrl.includes('token=')) {
                // Generate a random token
                var token = generateRandomString(24);

                // Append the token to the URL
                var newUrl = currentUrl + (currentUrl.includes('?') ? '&' : '?') + 'token=' + token;

                // Redirect to the new URL
                window.location.href = newUrl;
            }

            function generateRandomString(length) {
                var result = '';
                var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;

                for (var i = 0; i < length; i++) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }

                return result;
            }
        });
    </script>

    <!-- ... your HTML content ... -->
@endsection
