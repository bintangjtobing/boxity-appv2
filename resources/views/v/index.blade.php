<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="icon"
        href="https://res.cloudinary.com/boxity-id/image/upload/w_1000/q_auto:best/f_auto/v1678791753/asset_boxity/logo/icon-web_qusdsv.png"
        type="image/png">
    <link rel="stylesheet" href="{!! asset('custom.css') !!}">
    <title>{{ Route::currentRouteName() }} - Boxity for Internal</title>
</head>

<body>
    <main id="container-main" class="">
        <div style="min-height: 100vh" id="main-section"
            class="p-4 dark:bg-gray-900 h-auto pt-24 border overflow-hidden">
            @yield('container')
        </div>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.2.1/flowbite.min.js"></script>
</body>

</html>
