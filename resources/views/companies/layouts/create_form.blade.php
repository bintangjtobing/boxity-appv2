<section class="bg-gray-50 dark:bg-gray-900">
    <div class="mx-auto">
        <div class="bg-white dark:bg-gray-800 relative shadow-md sm:rounded-lg overflow-hidden">
            <h2 class="text-2xl dark:text-white mt-3 mx-4 font-medium">Add a new company</h2>
            <div class="flex flex-col md:flex-row items-center justify-between space-y-3 md:space-y-0 md:space-x-4 p-4">
                <div class="w-full">
                    <form action="{{ route('companies.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="grid gap-4 sm:grid-cols-2 sm:gap-6">
                            <div class="sm:col-span-2">
                                <label for="name"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Nama
                                    Usaha Bisnis / Perusahaan</label>
                                <input type="text" name="name" id="name"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="email"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Email
                                    Bisnis</label>
                                <input type="email" name="email" id="email"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="phone_number"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">No.
                                    Telepon</label>
                                <input type="phone" name="phone_number" id="phone_number"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="website"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Website</label>
                                <input type="url" name="website" id="website"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="https://yourcompany.com" required="">
                            </div>
                            <div class="sm:col-span-2">
                                <label for="address"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Address</label>
                                <input type="text" name="address" id="address"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="city"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">City</label>
                                <input type="text" list="cities" id="cityInput" name="city"
                                    placeholder="Pilih atau ketik untuk mencari kota..."
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <datalist id="cities" name="cities">
                                    <option value="" selected>Pilih kota</option>
                                    @foreach ($kota as $item)
                                        <option value="{{ $item['asciiname'] }}">{{ $item['asciiname'] }}</option>
                                    @endforeach
                                </datalist>
                            </div>

                            <div class="w-full">
                                <label for="province"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Provinsi</label>
                                <select id="province" name="province"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected disabled hidden>Pilih Provinsi</option>
                                    @foreach ($provinsi as $item)
                                        <option value="{{ $item['name'] }}">{{ $item['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="postal_code"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Postal
                                    Code</label>
                                <input type="text" name="postal_code" id="postal_code"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="" required="">
                            </div>
                            <div class="w-full">
                                <label for="country"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Negara</label>
                                <input type="text" name="country" id="country"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                                    placeholder="Indonesia" required="">
                            </div>
                            <div class="w-full">
                                <label for="industry"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Industry</label>
                                <select id="industry" name="industry"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih Industri</option>
                                    <option value="Pertanian">Pertanian</option>
                                    <option value="Perdagangan">Perdagangan</option>
                                    <option value="Manufaktur">Manufaktur</option>
                                    <option value="Teknologi Informasi dan Komunikasi (TIK)">Teknologi Informasi dan
                                        Komunikasi (TIK)</option>
                                    <option value="Pariwisata">Pariwisata</option>
                                    <option value="Energi">Energi</option>
                                    <option value="Konstruksi dan Properti">Konstruksi dan Properti</option>
                                    <option value="Keuangan">Keuangan</option>
                                    <option value="Kesehatan">Kesehatan</option>
                                    <option value="Pendidikan">Pendidikan</option>
                                    <option value="Transportasi dan Logistik">Transportasi dan Logistik</option>
                                    <option value="Industri Kreatif">Industri Kreatif</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </div>
                            <div class="w-full">
                                <label for="description"
                                    class="block mb-2 text-sm text-capitalize font-medium text-gray-900 dark:text-white">Tentang
                                    Usaha Bisnis</label>
                                <x-head.tinymce-config />
                                <textarea id="myeditorinstance" rows="10" tabindex="1"
                                    class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Write your thoughts here..." name="description"></textarea>
                            </div>
                        </div>
                        <button type="submit"
                            class="mt-5 flex items-center justify-center text-white bg-[#f95b12] hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus-ring-primary-800">
                            <svg class="h-3.5 w-3.5 text-gray-800 dark:text-white mr-2" aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 19">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M15 15h.01M4 12H2a1 1 0 0 0-1 1v4a1 1 0 0 0 1 1h16a1 1 0 0 0 1-1v-4a1 1 0 0 0-1-1h-3M9.5 1v10.93m4-3.93-4 4-4-4" />
                            </svg>
                            Save
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // Script untuk pencarian kota
    const cityInput = document.getElementById('cityInput');
    const citiesDatalist = document.getElementById('cities');

    cityInput.addEventListener('input', function() {
        const inputText = this.value.toLowerCase();
        const cityOptions = document.querySelectorAll('#cities option');
        cityOptions.forEach(option => {
            const optionText = option.value.toLowerCase();
            if (optionText.startsWith(inputText)) {
                option.hidden = false;
            } else {
                option.hidden = true;
            }
        });
    });

    citiesDatalist.addEventListener('change', function() {
        cityInput.value = this.value;
    });
</script>
