@extends('dashboard.layouts.main')
@section('container')
    <div class="grid grid-cols-5 gap-5">
        <div class="col-span-5">
            @include('companies.layouts.create_form')
        </div>
    </div>
@endsection
