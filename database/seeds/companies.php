<?php

use App\company;
use Illuminate\Database\Seeder;

class companies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        company::create([
            'id' => 1,
            'name' => 'Visiting Company',
            'email' => 'sales@boxity.id',
            'phone_number' => '081262845980',
            'website' => 'boxity.id',
            'logo' => '',
            'address' => 'Jl. Jend Jl. Jelambar Barat No.22-24 Lt 9 Unit O',
            'city' => 'Jakarta',
            'province' => 'DKI Jakarta',
            'postal_code' => '14240',
            'country' => 'Indonesia',
            'industry' => 'Teknologi Informasi dan Komunikasi (TIK)',
            'description' => '',
            'user_created' => 2,
            'user_updated'
        ]);
    }
}
