<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Bintang Tobing',
            'email' => 'bintang@boxity.id',
            'password' => Hash::make('libra2110'), // Hash password untuk keamanan
            'email_verified_at' => now(),
            'username' => 'bintangjtobing',
        ]);

        User::create([
            'name' => 'Admin Boxity',
            'email' => 'info@boxity.id',
            'password' => Hash::make('admin12345'),
            'email_verified_at' => now(),
            'username' => 'admin',
        ]);
    }
}
