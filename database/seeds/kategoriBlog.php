<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\kategori;

class kategoriBlog extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kategori::create([
            'id' => 1,
            'nama' => 'Knowledge',
        ]);
        kategori::create([
            'id' => 2,
            'nama' => 'News & Updates',
        ]);
        kategori::create([
            'id' => 3,
            'nama' => 'Business',
        ]);
    }
}
