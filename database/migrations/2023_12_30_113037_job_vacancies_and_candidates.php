<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JobVacanciesAndCandidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_vacancies', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->index('id');
            $table->string('title');
            $table->string('slug');
            $table->longText('description', 100000);
            $table->string('location');
            $table->integer('salary')->nullable();
            $table->date('deadline');
            $table->longText('requirement', 100000)->nullable();
            $table->longText('expected_from_candidate', 100000)->nullable();
            $table->longText('what_candidate_got', 100000)->nullable();
            $table->string('status');
            $table->timestamps();
        });

        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->integer('age');
            $table->Text('city');
            $table->string('phone_number');
            $table->integer('expected_sallary');
            $table->string('start_date');
            $table->string('resume');
            $table->string('cover_letter');
            $table->unsignedInteger('job_vacancy_id');
            $table->string('status');
            $table->timestamps();

            $table->foreign('job_vacancy_id')->references('id')->on('job_vacancies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('job_vacancies');
        Schema::dropIfExists('candidates');
    }
}
