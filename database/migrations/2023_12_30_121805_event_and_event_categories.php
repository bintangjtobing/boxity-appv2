<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EventAndEventCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public

    function

    up()
    {
        Schema::create('event_categories', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->index('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->timestamps();
        });
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');
            $table->date('start_date');
            $table->date('end_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('location');
            $table->string('image')->nullable();
            $table->string('status')->default('draft');
            $table->string('link_reg')->nullable();
            $table->timestamps();

            // Add foreign key to event_categories table
            $table->unsignedInteger('event_category_id')->nullable();
            $table->foreign('event_category_id')->references('id')->on('event_categories');
        });
    }

    public

    function

    down()
    {
        Schema::dropIfExists('event_categories');
        Schema::dropIfExists('events');
    }
}
