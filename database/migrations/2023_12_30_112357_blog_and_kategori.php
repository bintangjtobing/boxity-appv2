<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlogAndKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->index('id');
            $table->string('nama');
            $table->timestamps();
        });
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('slug');
            $table->longText('konten', 100000);
            $table->date('tanggal');
            $table->unsignedInteger('kategori_id')->nullable();
            $table->string('gambar_cover')->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('kategori_id')->references('id')->on('kategori');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kategori');
        Schema::dropIfExists('blogs');
    }
}
