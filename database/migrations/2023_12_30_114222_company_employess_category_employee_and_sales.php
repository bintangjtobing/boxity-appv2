<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyEmployessCategoryEmployeeAndSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create Companies Table
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->string('website')->nullable();
            $table->string('logo')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('province');
            $table->string('postal_code');
            $table->string('country');
            $table->string('industry');
            $table->longText('description')->nullable();
            $table->timestamps();
        });

        // Create Job Title Categories Table
        Schema::create('employees_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('description')->nullable();
            $table->timestamps();
        });

        // Create Employees Table
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('kode_anggota')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('job_title_category_id');
            $table->string('job_title');
            $table->string('employment_status');
            $table->date('hire_date');
            $table->date('termination_date')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('province');
            $table->string('postal_code');
            $table->string('country');
            $table->string('emergency_contact_name');
            $table->string('emergency_contact_phone_number');
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->index('kode_anggota');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('job_title_category_id')->references('id')->on('employees_categories');
        });

        // Create Sales Table
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('kode_sales')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('phone_number');
            $table->unsignedBigInteger('company_id');
            $table->string('territory');
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->longText('notes')->nullable();
            $table->string('bukti_foto_kunjungan')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('kode_sales')->references('kode_anggota')->on('employees');
        });

        // Table: vendors
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('pic_name');
            $table->string('email');
            $table->string('phone');
            $table->string('website')->nullable();
            $table->string('name_of_billing_address');
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('type')->enum(['customer', 'supplier']);
            $table->timestamps();
        });

        // table: items
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->decimal('price', 10, 2);
            $table->timestamps();
        });

        // Table: payment_methods
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        // Table: invoices
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('payment_method_id');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->string('invoice_number');
            $table->decimal('sub_total', 10, 2);
            $table->decimal('disc', 10, 2);
            $table->decimal('total_amount', 10, 2);
            $table->enum('status', ['viewed', 'sent', 'etc']);
            $table->enum('paid_status', ['unpaid', 'paid', 'etc']);
            $table->unsignedBigInteger('company_id');
            $table->timestamps();

            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        // Table: invoices_items
        Schema::create('invoices_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id'); // Change this to bigInteger or unsignedBigInteger
            $table->unsignedBigInteger('item_id');
            $table->integer('quantity');
            $table->decimal('price', 10, 2);
            $table->decimal('tax', 10, 2);
            $table->decimal('discount', 10, 2);
            $table->decimal('discount_val', 10, 2);
            $table->unsignedBigInteger('company_id'); // Change this to bigInteger or unsignedBigInteger
            $table->timestamps();

            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('company_id')->references('id')->on('companies');
        });
        Schema::create('invoice_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        // Table: expenses
        Schema::create('expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_category_id');
            $table->date('date');
            $table->decimal('amount', 10, 2);
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('payment_method_id');
            $table->text('note')->nullable();
            $table->string('receipt')->nullable();
            $table->unsignedBigInteger('company_id');
            $table->timestamps();

            $table->foreign('invoice_category_id')->references('id')->on('invoice_category');
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
        Schema::dropIfExists('invoices_items');
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('payment_methods');
        Schema::dropIfExists('items');
        Schema::dropIfExists('vendors');
        Schema::dropIfExists('sales');
        Schema::dropIfExists('employees');
        Schema::dropIfExists('employees_categories');
        Schema::dropIfExists('companies');
    }
}
