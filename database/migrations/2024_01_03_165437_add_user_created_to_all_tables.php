<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserCreatedToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['blogs', 'candidates', 'companies', 'employees', 'employees_categories', 'event_categories', 'events', 'job_vacancies', 'kategori', 'sales'];

        foreach ($tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->integer('user_created')->unsigned()->nullable();
                $table->integer('user_updated')->unsigned()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = ['blogs', 'candidates', 'companies', 'employees', 'employees_categories', 'event_categories', 'events', 'job_vacancies', 'kategori', 'sales'];

        foreach ($tables as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn('user_created');
                $table->dropColumn('user_updated');
            });
        }
    }
}
