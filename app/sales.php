<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class sales extends Model
{
    protected $table = 'sales';

    protected

        $fillable = [
            'kode_sales',
            'name',
            'email',
            'phone_number',
            'company_id',
            'territory',
            'date',
            'time',
            'notes',
            'bukti_foto_kunjungan',
            'user_created',
            'user_updated',
            'status'
        ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }
    public function company()
    {
        return $this->belongsTo(company::class, 'company_id');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
    public function salesConnected()
    {
        return $this->belongsTo('App\employee', 'kode_sales', 'kode_anggota');
    }
}
