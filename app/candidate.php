<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class candidate extends Model
{
    protected $table = 'candidates';
    protected $fillable = [
        'name',
        'email',
        'age',
        'city',
        'expected_sallary',
        'start_date',
        'phone_number',
        'resume',
        'cover_letter',
        'job_vacancy_id',
        'status',
        'user_created',
        'user_updated'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }
    public function job_vacancy()
    {
        return $this->belongsTo('App\jobVacancy');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}
