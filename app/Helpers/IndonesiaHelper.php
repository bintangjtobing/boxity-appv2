<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class IndonesiaHelper
{
    public static function getDataCity()
    {
        $response = Http::withHeaders([
            'X-Parse-Application-Id' => 'gW8QymawoxVTlZ1Km3yim9T5GftEPKecyXOj19JO', // Application ID Anda
            'X-Parse-REST-API-Key' => 'FNvx6weYr9AX60lvuMZ4LhUxLK32Tw3xEULjs12E', // REST API Key Anda
        ])->get('https://parseapi.back4app.com/classes/Indonesiacities_Indonesia_Cities_Database', [
            'limit' => 430,
            'keys' => 'asciiname,country_code,timezone',
        ]);
        $decodedResponse = json_decode($response->body(), true); // Ubah respons HTTP menjadi array asosiatif

        return $decodedResponse['results']; // Kembalikan array asosiatif yang berisi data provinsi
    }
    public static function getProvince()
    {
        $response = Http::get('https://emsifa.github.io/api-wilayah-indonesia/api/provinces.json');
        $decodedResponse = json_decode($response->body(), true); // Ubah respons HTTP menjadi array asosiatif

        return $decodedResponse; // Kembalikan array asosiatif yang berisi data provinsi
    }
    public static function getProvinceById($provinceId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/provinces/' . $provinceId . '.json';
        $response = Http::get($url);
        return $response->json($response);
    }
    public function getRegencies($provinceId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/regencies/' . $provinceId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
    public function getRegenciesById($regencyId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/regencies/' . $regencyId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
    public function getDistrict($regencyId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/districts/' . $regencyId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
    public function getDistrictById($districtId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/districts/' . $districtId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
    public function getVillages($districtId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/villages/' . $districtId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
    public function getVillagesById($villagesId)
    {
        $url = 'https://emsifa.github.io/api-wilayah-indonesia/api/villages/' . $villagesId . '.json';

        $response = Http::get($url);

        return $response->json($response);
    }
}
