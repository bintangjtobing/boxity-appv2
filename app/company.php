<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class company extends Model
{
    protected $table = 'companies';
    protected

        $fillable = [
            'name',
            'email',
            'phone_number',
            'website',
            'logo',
            'address',
            'city',
            'province',
            'postal_code',
            'country',
            'industry',
            'description',
            'user_created',
            'user_updated'
        ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}
