<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class event extends Model
{
    protected $table = 'events';
    protected $fillable = [
        'name',
        'slug',
        'description',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'location',
        'image',
        'status',
        'event_category_id',
        'user_created',
        'user_updated'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->slug = Str::slug($model->name, '-');
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }

    public function eventCategory()
    {
        return $this->belongsTo(eventCategories::class);
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}