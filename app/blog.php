<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = [
        'judul',
        'slug',
        'konten',
        'tanggal',
        'kategori_id',
        'gambar_cover',
        'status',
        'user_created',
        'user_updated'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->slug = Str::slug($model->judul, '-');
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }

    public function kategori()
    {
        return $this->belongsTo('App\kategori');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}
