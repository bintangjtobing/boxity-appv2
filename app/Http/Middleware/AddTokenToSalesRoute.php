<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class AddTokenToSalesRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Generate a random token
        $token = Str::random(60); // Use Str::random(60) in Laravel 6 or earlier

        // Append the token to the route
        $request->attributes->add(['token' => $token]);

        return $next($request);
    }
}
