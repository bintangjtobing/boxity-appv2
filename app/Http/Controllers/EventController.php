<?php

namespace App\Http\Controllers;

use App\event;
use App\eventCategories;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
{
    $events = Event::latest()->with('eventCategory')->paginate(10);
    $categories = EventCategories::latest()->with('events')->withCount('events')->paginate(10);
    return view('events.index', compact('events', 'categories'));
}

    public function create()
    {
        $eventCategories = eventCategories::all();
        return view('events.create', compact('eventCategories'));
    }

    public function store(Request $request)
    {
        $event = new Event();
        $event->name = $request->name;
        $event->slug = $request->slug;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->start_time = $request->start_time;
        $event->end_time = $request->end_time;
        $event->location = $request->location;
        if ($request->file('image')) {
            $event->image = cloudinary()->upload(request()->file('image')->getRealPath())->getSecurePath();
        }
        $event->link_reg = $request->link_reg;
        $event->status = 'draft';
        $event->event_category_id = $request->event_category_id;
        $event->save();

        return redirect('/events')->with('success', 'Event berhasil dibuat.');
    }

    public function show(Event $event)
    {
        return view('events.show', compact('event'));
    }

    public function edit(Event $event)
    {
        $eventCategories = eventCategories::all();

        return view('events.edit', compact('event', 'eventCategories'));
    }

    public function update(Request $request, Event $event)
    {

        $event->name = $request->name;
        $event->slug = $request->slug;
        $event->description = $request->description;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->start_time = $request->start_time;
        $event->end_time = $request->end_time;
        $event->location = $request->location;
        $event->image = $request->image;
        $event->link_reg = $request->link_reg;
        $event->status = $request->status;
        $event->event_category_id = $request->event_category_id;
        $event->save();

        return redirect('/events')->with('success', 'Event berhasil diperbarui.');
    }

    public function destroy(Event $event, $slug)
    {
        try {
            $event = event::where('slug', $slug)->first();
            $event->delete();
            return redirect('/events')->with('success', 'Event berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/events')->with('error', 'Terjadi kesalahan saat menghapus Event: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/events')->with('error', 'Terjadi kesalahan saat menghapus Event: ' . $e->getMessage());
        }
    }
}