<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $user = User::where('email', $data['email'])->first();
        if ($user && Hash::check($data['password'], $user->password)) {
            $request->session()->regenerate();
            $user->update(['last_login_at' => now()]);
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => TRUE])) {
            // Authentication passed...
            return redirect()->intended('/');
        }

        return back()->with('alert-danger', [
            'title' => 'Email atau password salah / User belum diotorisasi',
            'message' => 'Silakan coba lagi.',
        ]);
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $user->sendEmailVerificationNotification();

        return redirect('/login')->with('success', 'Registrasi berhasil. Silahkan verifikasi email Anda untuk melanjutkan.');
    }

    public function forgotPassword(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
        ]);

        $user = User::where('email', $data['email'])->first();

        if ($user) {
            $user->sendPasswordResetNotification();
            return redirect('/login')->with('success', 'Sebuah email telah dikirim ke alamat email Anda untuk reset password.');
        }

        return back()->withErrors([
            'email' => 'Email tidak terdaftar.',
        ]);
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return redirect('/login');
    }
}
