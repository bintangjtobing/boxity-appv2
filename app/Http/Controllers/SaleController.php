<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sales;
use App\company;
use App\employee;
use App\Helpers\IndonesiaHelper;
use Illuminate\Database\QueryException;

class SaleController extends Controller
{
    public function globalIndex(Request $request)
    {
        $token = $request->get('token');
        $users = employee::with('jobTitleCategory')
            ->whereHas('jobTitleCategory', function ($query) {
                $query->where('name', 'LIKE', '%sales%');
            })
            ->latest()
            ->get();
        $companies = company::find(1);
        $kota = IndonesiaHelper::getDataCity();
        $provinsi = IndonesiaHelper::getProvince();
        return view('v.sales.index', compact('users', 'companies', 'kota', 'provinsi'));
        // return response()->json($users);
    }
    public function index(Request $request)
    {
        $sales = sales::with(['company', 'salesConnected'])->latest()->paginate(10);
        return view('sales.index', compact('sales'));
        // return response()->json($sales);
    }

    public function create()
    {
        $companies = Company::all();

        return view('sales.create', compact('companies'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:sales',
            'phone_number' => 'required|string|max:255',
            'company_id' => 'required|exists:companies,id',
            'territory' => 'required|string',
            'date' => 'required|date',
            'time' => 'required|date_format:H:i',
        ]);

        $sale = new sales();
        $sale->name = $request->name;
        $sale->email = $request->email;
        $sale->phone_number = $request->phone_number;
        $sale->company_id = $request->company_id;
        $sale->territory = $request->territory;
        $sale->date = $request->date;
        $sale->time = $request->time;
        $sale->notes = $request->notes;
        $sale->bukti_foto_kunjungan = $request->bukti_foto_kunjungan;
        $sale->save();

        return redirect()->route('sales.index')->with('success', 'Sales berhasil dibuat.');
    }
    public function storeGlobalIndex(Request $request)
    {
        $token = $request->get('token');
        $sale = new sales();
        $sale->kode_sales = $request->kode_sales;
        $sale->name = $request->name;
        $sale->email = $request->email;
        $sale->phone_number = $request->phone_number;
        $sale->company_id = 1;
        $sale->territory = $request->territory;
        $sale->date = $request->date;
        $sale->time = $request->time;
        $sale->notes = $request->notes;
        if ($request->file('bukti_foto_kunjungan')) {
            $sale->bukti_foto_kunjungan = cloudinary()->upload(request()->file('bukti_foto_kunjungan')->getRealPath())->getSecurePath();
        }
        $sale->status = 0;
        $sale->save();

        return redirect('/v/sales')->with('success', 'Sales Prospect berhasil dikirim dan sedang menunggu verifikasi.');
        // return response()->json($request->all());
    }

    public function show(sales $sale)
    {
        return view('sales.show', compact('sale'));
    }

    public function edit(sales $sale)
    {
        $companies = Company::all();

        return view('sales.edit', compact('sale', 'companies'));
    }

    public function update(Request $request, sales $sale)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:sales,email,' . $sale->id,
            'phone_number' => 'required|string|max:255',
            'company_id' => 'required|exists:companies,id',
            'territory' => 'required|string',
            'date' => 'required|date',
            'time' => 'required|date_format:H:i',
        ]);

        $sale->name = $request->name;
        $sale->email = $request->email;
        $sale->phone_number = $request->phone_number;
        $sale->company_id = $request->company_id;
        $sale->territory = $request->territory;
        $sale->date = $request->date;
        $sale->time = $request->time;
        $sale->notes = $request->notes;
        $sale->bukti_foto_kunjungan = $request->bukti_foto_kunjungan;
        $sale->save();

        return redirect()->route('sales.index')->with('success', 'Sales berhasil diperbarui.');
    }

    public function destroy(sales $id)
    {
        try {
            $event = sales::find($id)->first();
            $event->delete();
            return redirect('/sales')->with('success', 'Sale berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/sales')->with('error', 'Terjadi kesalahan saat menghapus Sale: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/sales')->with('error', 'Terjadi kesalahan saat menghapus Sale: ' . $e->getMessage());
        }
    }
}
