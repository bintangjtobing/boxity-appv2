<?php

namespace App\Http\Controllers;

use App\candidate;
use App\jobVacancy;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function index()
    {
        $candidates = Candidate::latest()->with('job_vacancy')->paginate(10);
        return view('candidates.index', compact('candidates'));
        // return response()->json($candidates);
    }

    public function create()
    {
        $job = jobVacancy::latest()->get();
        return view('candidates.create', compact('job'));
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|string|max:255',
        //     'email' => 'required|email|unique:candidates',
        //     'phone_number' => 'required|string|max:255',
        //     'resume' => 'required|string',
        //     'cover_letter' => 'required|string',
        //     'job_vacancy_id' => 'required|integer',
        //     'status' => 'required|string',
        // ]);

        $candidate = new Candidate();
        $candidate->name = $request->name;
        $candidate->email = $request->email;
        $candidate->age = $request->age;
        $candidate->city = $request->city;
        $candidate->expected_sallary = $request->expected_sallary;
        $candidate->start_date = $request->start_date;
        $candidate->phone_number = $request->phone_number;
        if ($request->file('resume')) {
            $candidate->resume = cloudinary()->upload(request()->file('resume')->getRealPath())->getSecurePath();
        }
        $candidate->cover_letter = $request->cover_letter;
        $candidate->job_vacancy_id = $request->job_vacancy_id;
        $candidate->status = 'draft';
        $candidate->save();

        return redirect('/candidates')->with('success', 'Kandidat berhasil dibuat.');
    }

    public function show(Candidate $candidate)
    {
        return view('candidates.show', compact('candidate'));
    }

    public function edit(Candidate $candidate)
    {
        return view('candidates.edit', compact('candidate'));
    }

    public function update(Request $request, Candidate $candidate)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:candidates,email,' . $candidate->id,
            'phone_number' => 'required|string|max:255',
            'resume' => 'required|string',
            'cover_letter' => 'required|string',
            'job_vacancy_id' => 'required|integer',
            'status' => 'required|string',
        ]);

        $candidate->name = $request->name;
        $candidate->email = $request->email;
        $candidate->age = $request->age;
        $candidate->city = $request->city;
        $candidate->expected_sallary = $request->expected_sallary;
        $candidate->start_date = $request->start_date;
        $candidate->phone_number = $request->phone_number;
        $candidate->resume = $request->resume;
        $candidate->cover_letter = $request->cover_letter;
        $candidate->job_vacancy_id = $request->job_vacancy_id;
        $candidate->status = $request->status;
        $candidate->save();

        return redirect()->route('candidates.index')->with('success', 'Kandidat berhasil diperbarui.');
    }

    public function destroy(Candidate $candidate)
    {
        try {
            $candidate->delete();
            return redirect('/candidates')->with('success', 'candidates berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/candidates')->with('error', 'Terjadi kesalahan saat menghapus candidates: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/candidates')->with('error', 'Terjadi kesalahan saat menghapus candidates: ' . $e->getMessage());
        }
    }
}
