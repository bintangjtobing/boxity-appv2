<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jobVacancy;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class JobVacancyController extends Controller
{
    public function index()
    {
        $jobvacancies = JobVacancy::latest()->paginate(10);

        return view('jobvacancies.index', compact('jobvacancies'));
    }

    public function create()
    {
        return view('jobvacancies.create');
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'title' => 'required|string|max:255',
        //     'description' => 'required|string',
        //     'location' => 'required|string|max:255',
        //     'salary' => 'nullable',
        //     'deadline' => 'required',
        //     'requirement' => 'nullable',
        //     'expected_from_candidate' => 'nullable',
        //     'what_candidate_got' => 'nullable',
        //     'status' => 'required',
        // ]);
        $nextId = DB::table('job_vacancies')->max('id') + 1;
        $jobvacancy = new JobVacancy();
        $jobvacancy->id = $nextId;
        $jobvacancy->title = $request->title;
        $jobvacancy->description = $request->description;
        $jobvacancy->location = $request->location;
        $jobvacancy->salary = $request->salary;
        $jobvacancy->deadline = $request->deadline;
        $jobvacancy->requirement = $request->requirement;
        $jobvacancy->expected_from_candidate = $request->expected_from_candidate;
        $jobvacancy->what_candidate_got = $request->what_candidate_got;
        $jobvacancy->status = 'published';
        $jobvacancy->save();
        // return response()->json($jobvacancy);
        return redirect('/jobvacancies')->with('success', 'Lowongan kerja berhasil dibuat.');
    }

    public function show(JobVacancy $jobvacancy)
    {
        return view('jobvacancies.show', compact('jobvacancy'));
    }

    public function edit(JobVacancy $jobvacancy)
    {
        return view('jobvacancies.edit', compact('jobvacancy'));
    }

    public function update(Request $request, JobVacancy $jobvacancy)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'location' => 'required|string|max:255',
            'salary' => 'nullable|integer',
            'deadline' => 'required|date',
            'requirement' => 'nullable|string',
            'expected_from_candidate' => 'nullable|string',
            'what_candidate_got' => 'nullable|string',
            'status' => 'required|string',
        ]);

        $jobvacancy->title = $request->title;
        $jobvacancy->description = $request->description;
        $jobvacancy->location = $request->location;
        $jobvacancy->salary = $request->salary;
        $jobvacancy->deadline = $request->deadline;
        $jobvacancy->requirement = $request->requirement;
        $jobvacancy->expected_from_candidate = $request->expected_from_candidate;
        $jobvacancy->what_candidate_got = $request->what_candidate_got;
        $jobvacancy->status = $request->status;
        $jobvacancy->save();

        return redirect()->route('jobvacancies.index')->with('success', 'Lowongan kerja berhasil diperbarui.');
    }

    public function destroy(JobVacancy $jobvacancy)
    {
        try {
            $jobvacancy->delete();
            return redirect('/jobvacancies')->with('success', 'Lowongan kerja berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/jobvacancies')->with('error', 'Terjadi kesalahan saat menghapus Lowongan kerja: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/jobvacancies')->with('error', 'Terjadi kesalahan saat menghapus Lowongan kerja: ' . $e->getMessage());
        }
    }
}
