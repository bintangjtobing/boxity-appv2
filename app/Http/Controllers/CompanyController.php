<?php

namespace App\Http\Controllers;

use App\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\IndonesiaHelper;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::latest()->paginate(10);

        return view('companies.index', compact('companies'));
    }

    public function create()
    {
        $kota = IndonesiaHelper::getDataCity();
        $provinsi = IndonesiaHelper::getProvince();
        // dd($provinsi);
        return view('companies.create', [
            'kota' => $kota,
            'provinsi' => $provinsi
        ]);
    }

    public function store(Request $request)
    {
        $nextId = DB::table('companies')->max('id') + 1;
        $company = new Company();
        $company->id = $nextId;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->phone_number = $request->phone_number;
        $company->website = $request->website;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->province = $request->province;
        $company->postal_code = $request->postal_code;
        $company->country = $request->country;
        $company->industry = $request->industry;
        $company->description = $request->description;
        $company->save();

        return redirect('/companies')->with('success', 'Perusahaan berhasil dibuat.');
    }

    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    public function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }

    public function update(Request $request, Company $company)
    {
        $company->name = $request->name;
        $company->email = $request->email;
        $company->phone_number = $request->phone_number;
        $company->website = $request->website;
        $company->logo = $request->logo;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->province = $request->province;
        $company->postal_code = $request->postal_code;
        $company->country = $request->country;
        $company->industry = $request->industry;
        $company->description = $request->description;
        $company->save();

        return redirect('/companies')->with('success', 'Perusahaan berhasil diperbarui.');
    }

    public function destroy(Company $company, $id)
    {
        try {
            $company = company::find($id);
            $company->delete();
            return redirect('/companies')->with('success', 'companies berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/companies')->with('error', 'Terjadi kesalahan saat menghapus companies: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/companies')->with('error', 'Terjadi kesalahan saat menghapus companies: ' . $e->getMessage());
        }
    }
}
