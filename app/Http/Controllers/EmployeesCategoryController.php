<?php

namespace App\Http\Controllers;

use App\employee_kategori;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeesCategoryController extends Controller
{
    public function index()
    {
        $employeesCategories = employee_kategori::latest()->paginate(10);

        return view('employees.index', compact('employeesCategories'));
    }

    public function create()
    {
        return view('employees_categories.create');
    }

    public function store(Request $request)
    {
        $nextId = DB::table('employees_categories')->max('id') + 1;
        $employeesCategory = new employee_kategori();
        $employeesCategory->id = $nextId;
        $employeesCategory->name = $request->name;
        $employeesCategory->description = $request->description;
        $employeesCategory->save();

        return redirect('/employees')->with('success', 'Kategori karyawan berhasil dibuat.');
    }

    public function show(employee_kategori $employeesCategory)
    {
        return view('employees_categories.show', compact('employeesCategory'));
    }

    public function edit(employee_kategori $employeesCategory)
    {
        return view('employees_categories.edit', compact('employeesCategory'));
    }

    public function update(Request $request, employee_kategori $employeesCategory)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $employeesCategory->name = $request->name;
        $employeesCategory->description = $request->description;
        $employeesCategory->save();

        return redirect('/employees')->with('success', 'Kategori karyawan berhasil diperbarui.');
    }

    public function destroy(employee_kategori $employeesCategory)
    {
        try {
            $employeesCategory->delete();
            return redirect('/employees')->with('success', 'Kategori karyawan berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/employees')->with('error', 'Terjadi kesalahan saat menghapus Kategori karyawan: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/employees')->with('error', 'Terjadi kesalahan saat menghapus Kategori karyawan: ' . $e->getMessage());
        }
    }
}
