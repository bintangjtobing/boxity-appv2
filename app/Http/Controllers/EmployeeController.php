<?php

namespace App\Http\Controllers;

use App\employee;
use App\employee_kategori;
use App\company;
use App\Helpers\IndonesiaHelper;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $employees = Employee::with(['company', 'jobTitleCategory'])->latest()->paginate(10);
        $employeesCategories = employee_kategori::latest()->paginate(10);
        return view('employees.index', compact('employees', 'employeesCategories'));

        // return response()->json($employees);
    }

    public function create()
    {
        $kota = IndonesiaHelper::getDataCity();
        $provinsi = IndonesiaHelper::getProvince();
        $companies = Company::all();
        $jobCategories = employee_kategori::all();
        return view('employees.create', compact('companies', 'jobCategories', 'kota', 'provinsi'));
    }

    public function store(Request $request)
    {

        $employee = new Employee();
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone_number = $request->phone_number;
        $employee->company_id = $request->company_id;
        $employee->job_title_category_id = $request->job_title_category_id;
        $employee->job_title = $request->job_title;
        $employee->employment_status = $request->employment_status;
        $employee->hire_date = $request->hire_date;
        $employee->termination_date = $request->termination_date;
        $employee->address = $request->address;
        $employee->city = $request->city;
        $employee->province = $request->province;
        $employee->postal_code = $request->postal_code;
        $employee->country = $request->country;
        $employee->emergency_contact_name = $request->emergency_contact_name;
        $employee->emergency_contact_phone_number = $request->emergency_contact_phone_number;
        $employee->notes = $request->notes;
        $employee->save();

        return redirect('/employees')->with('success', 'Karyawan berhasil dibuat.');
    }

    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }

    public function edit(Employee $employee)
    {
        $companies = Company::all();
        $jobTitleCategories = employee_kategori::all();

        return view('employees.edit', compact('employee', 'companies', 'jobTitleCategories'));
    }
    public function update(Request $request, Employee $employee)
    {
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone_number = $request->phone_number;
        $employee->company_id = $request->company_id;
        $employee->job_title_category_id = $request->job_title_category_id;
        $employee->job_title = $request->job_title;
        $employee->employment_status = $request->employment_status;
        $employee->hire_date = $request->hire_date;
        $employee->termination_date = $request->termination_date;
        $employee->address = $request->address;
        $employee->city = $request->city;
        $employee->province = $request->province;
        $employee->postal_code = $request->postal_code;
        $employee->country = $request->country;
        $employee->emergency_contact_name = $request->emergency_contact_name;
        $employee->emergency_contact_phone_number = $request->emergency_contact_phone_number;
        $employee->notes = $request->notes;
        $employee->save();

        return redirect('/employees')->with('success', 'Karyawan berhasil diperbarui.');
    }

    public function destroy(Employee $employee)
    {
        try {
            $employee->delete();
            return redirect('/employees')->with('success', 'Karyawan berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/employees')->with('error', 'Terjadi kesalahan saat menghapus karyawan: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/employees')->with('error', 'Terjadi kesalahan saat menghapus karyawan: ' . $e->getMessage());
        }
    }
}
