<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\blog;
use Cloudinary;
use App\kategori;
use Illuminate\Database\QueryException;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = blog::latest()->with('kategori')->paginate(10);
        $kategoris = kategori::latest()->with('blogs')->withCount('blogs')->paginate(10);
        return view('blogs.index', compact('blogs', 'kategoris'));
        // return response()->json(compact('blogs', 'kategoris'));
    }

    public function create()
    {
        $kategories = Kategori::latest()->get();
        return view('blogs.create', compact('kategories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|string|max:255',
            'konten' => 'required|string',
        ]);

        $blog = new Blog();
        $blog->judul = $request->judul;
        $blog->konten = $request->konten;
        $blog->tanggal = date(now());
        $blog->kategori_id = $request->kategori_id;
        if ($request->file('gambar_cover')) {
            $blog->gambar_cover = cloudinary()->upload(request()->file('gambar_cover')->getRealPath())->getSecurePath();
        }
        $blog->status = 'draft';
        $blog->save();

        return redirect('/blogs')->with('success', 'Blog berhasil dibuat.');
        // return response()->json($blog);
    }

    public function show(Blog $blog)
    {
        return view('blogs.show', compact('blog'));
    }

    public function edit(Blog $blog)
    {
        return view('blogs.edit', compact('blog'));
    }

    public function update(Request $request, Blog $blog)
    {
        $request->validate([
            'judul' => 'required|string|max:255',
            'konten' => 'required|string',
            'tanggal' => 'required|date',
            'kategori_id' => 'nullable|integer',
            'gambar_cover' => 'nullable|image',
            'status' => 'required|string|in:published,draft',
        ]);

        $blog->judul = $request->judul;
        $blog->konten = $request->konten;
        $blog->tanggal = $request->tanggal;
        $blog->kategori_id = $request->kategori_id;
        if ($request->file('gambar_cover')) {
            $blog->gambar_cover = $request->file('gambar_cover')->store('blogs');
        }
        $blog->status = $request->status;
        $blog->save();

        return redirect()->route('blogs.index')->with('success', 'Blog berhasil diperbarui.');
    }

    public function destroy(Blog $blog)
    {
        try {
            $blog->delete();
            return redirect('/blogs')->with('success', 'Blog berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/blogs')->with('error', 'Terjadi kesalahan saat menghapus Blog: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/blogs')->with('error', 'Terjadi kesalahan saat menghapus Blog: ' . $e->getMessage());
        }
    }
}
