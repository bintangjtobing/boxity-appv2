<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function index()
    {
        $kategoris = Kategori::latest()->paginate(10);

        return view('kategoris.index', compact('kategoris'));
    }

    public function create()
    {
        return view('kategoris.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'string|unique:kategori',
        ]);
        $nextId = DB::table('kategori')->max('id') + 1;  // Dapatkan ID baru
        $kategori = new Kategori();  // Buat instance model Kategori
        $kategori->id = $nextId;  // Set ID pada objek
        $kategori->nama = $request->nama;  // Set nama pada objek
        $kategori->save();  // Simpan objek ke database
        // return response()->json($request->all());
        return redirect('/blogs')->with('success', 'Kategori berhasil dibuat.');
    }

    public function show(Kategori $kategori)
    {
        return view('kategoris.show', compact('kategori'));
    }

    public function edit(Kategori $kategori)
    {
        return view('kategoris.edit', compact('kategori'));
    }

    public function update(Request $request, Kategori $kategori)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
        ]);

        $kategori->nama = $request->nama;
        $kategori->save();

        return redirect('/blogs')->with('success', 'Kategori berhasil diperbarui.');
    }

    public function destroy(Kategori $kategori, $id)
    {
        $kategori = kategori::find($id);
        $kategori->delete();

        return redirect('/blogs')->with('success', 'Kategori berhasil dihapus.');
    }

    public function listBlogs(Kategori $kategori)
    {
        $blogs = $kategori->blogs()->latest()->paginate(10);

        return view('kategoris.blogs', compact('blogs'));
    }
}
