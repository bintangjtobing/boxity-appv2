<?php

namespace App\Http\Controllers;

use App\eventCategories;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventCategoryController extends Controller
{
    public function index()
    {
        $eventCategories = eventCategories::latest()->paginate(10);

        return view('event_categories.index', compact('eventCategories'));
    }

    public function create()
    {
        return view('event_categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);
        $nextId = DB::table('job_vacancies')->max('id') + 1;

        $eventCategory = new eventCategories();
        $eventCategory->id = $nextId;
        $eventCategory->name = $request->name;
        $eventCategory->description = $request->description;
        $eventCategory->save();

        return redirect('/events')->with('success', 'Kategori event berhasil dibuat.');
    }

    public function show(eventCategories $eventCategory)
    {
        return view('event_categories.show', compact('eventCategory'));
    }

    public function edit(eventCategories $eventCategory)
    {
        return view('event_categories.edit', compact('eventCategory'));
    }

    public function update(Request $request, eventCategories $eventCategory)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);

        $eventCategory->name = $request->name;
        $eventCategory->description = $request->description;
        $eventCategory->save();

        return redirect()->route('event_categories.index')->with('success', 'Kategori event berhasil diperbarui.');
    }

    public function destroy(eventCategories $eventCategory, $slug)
    {
        try {
            $event = eventCategories::where('slug', $slug)->first();
            $event->delete();
            return redirect('/events')->with('success', 'Kategori event berhasil dihapus.');
        } catch (QueryException $e) {
            return redirect('/events')->with('error', 'Terjadi kesalahan saat menghapus Kategori event: ' . $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/events')->with('error', 'Terjadi kesalahan saat menghapus Kategori event: ' . $e->getMessage());
        }
    }
}
