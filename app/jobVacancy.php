<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class jobVacancy extends Model
{
    protected $table = 'job_vacancies';
    protected $fillable = [
        'title',
        'slug',
        'description',
        'location',
        'salary',
        'deadline',
        'requirement',
        'expected_from_candidate',
        'what_candidate_got',
        'status',
        'user_created',
        'user_updated'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->slug = Str::slug($model->title, '-');
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }

    public function candidates()
    {
        return $this->hasMany('candidate');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}
