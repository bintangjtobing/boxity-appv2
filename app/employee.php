<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class employee extends Model
{
    protected $table = 'employees';
    protected

        $fillable = [
            'name',
            'kode_anggota',
            'email',
            'phone_number',
            'company_id',
            'job_title_category_id',
            'job_title',
            'employment_status',
            'hire_date',
            'termination_date',
            'address',
            'city',
            'province',
            'postal_code',
            'country',
            'emergency_contact_name',
            'emergency_contact_phone_number',
            'notes',
            'user_created',
            'user_updated'
        ];
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_created = Auth::id();
            $model->user_updated = Auth::id();
            $model->kode_anggota = $model->generateKodeAnggota();
        });

        static::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }

    // Method untuk generate kode anggota
    protected function generateKodeAnggota()
    {
        // Gunakan format kode anggota yang diinginkan
        try {
            return date('Ym') . mt_rand(0, 9999);
        } catch (\Exception $e) {
            // Handle kesalahan jika diperlukan
            // Misalnya, dapat mencetak pesan kesalahan atau melakukan tindakan lain
            return null;
        }
    }
    public function company()
    {
        return $this->belongsTo('App\company');
    }

    public function jobTitleCategory()
    {
        return $this->belongsTo(employee_kategori::class, 'job_title_category_id');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
    // Menyesuaikan relasi pada model Sales
    public function salesConnected()
    {
        return $this->hasMany('sales');
    }
}
