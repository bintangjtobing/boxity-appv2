<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = [
        'nama',
        'user_created',
        'user_updated'
    ];
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->user_created = Auth::id();
        });
        self::updating(function ($model) {
            $model->user_updated = Auth::id();
        });
    }
    public function blogs()
    {
        return $this->hasMany('app\blog');
    }
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'user_created');
    }
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'user_updated');
    }
}
